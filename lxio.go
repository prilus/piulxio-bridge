package piulxiobridge

import (
	"context"
	"fmt"
	"log"
	"os"
	"sync/atomic"
	"time"

	"github.com/google/gousb"
)

var logger = log.New(os.Stdout, "bridge ", log.LstdFlags|log.Lshortfile)

type LXIOHandler struct {
	ctx         context.Context
	usbCtx      *gousb.Context
	usbDev      *gousb.Device
	usbIntfDone func()

	sensorData atomic.Pointer[LXIOSwitch]
	lightData  atomic.Pointer[LXIOLight]
}

type LXIOSwitchLightable struct {
	P13 bool
	P11 bool
	P15 bool
	P19 bool
	P17 bool

	P23 bool
	P21 bool
	P25 bool
	P29 bool
	P27 bool

	P1Hand3 bool
	P1Hand1 bool
	P1Hand5 bool
	P1Hand9 bool
	P1Hand7 bool

	P2Hand3 bool
	P2Hand1 bool
	P2Hand5 bool
	P2Hand9 bool
	P2Hand7 bool
}

type LXIOLight struct {
	LXIOSwitchLightable

	CoinReceived bool

	Unk1        bool // maybe cabinet light?
	Unk2        bool // maybe cabinet light?
	CoinEnable1 bool
	CoinEnable2 bool
	CoinEnable3 bool
	Unk6        bool // maybe cabinet light?
	Unk7        bool // maybe cabinet light?
	Unk8        bool // maybe cabinet light?
}

type LXIOSwitch struct {
	LXIOSwitchLightable

	Clear   bool
	Service bool
	Test    bool
	Coin    bool
	// Coin2 bool // ?
}

func NewLXIOHandler(ctx context.Context) (*LXIOHandler, error) {
	v := &LXIOHandler{
		ctx: ctx,
	}

	v.sensorData.Store(new(LXIOSwitch))
	v.lightData.Store(new(LXIOLight))

	if err := v.open(); err != nil {
		return nil, err
	}

	go v.loop()

	return v, nil
}

func (t *LXIOHandler) open() error {
	usb := gousb.NewContext()
	defer usb.Close()

	usb.Debug(3)

	logger.Println("usb context created")

	dev, err := usb.OpenDeviceWithVIDPID(_LXIO_VID, _LXIO_PID)
	if err != nil {
		logger.Println(err)
		return err
	}
	if dev == nil {
		err = fmt.Errorf("lxio device not found")
		logger.Println(err)
		return err
	}

	logger.Println("lxio device opened")

	if err := dev.SetAutoDetach(true); err != nil {
		logger.Println(err)
		return err
	}

	logger.Println("lxio device auto detach set")

	_, done, err := dev.DefaultInterface()
	if err != nil {
		logger.Println(err)
		return err
	}

	logger.Println("lxio device default interface set")

	t.usbCtx = usb
	t.usbDev = dev
	t.usbIntfDone = done

	return nil
}

func (t *LXIOHandler) Close() {
	if t.usbIntfDone != nil {
		t.usbIntfDone()
		t.usbIntfDone = nil
	}

	if t.usbDev != nil {
		t.usbDev.Close()
		t.usbDev = nil
	}

	if t.usbCtx != nil {
		t.usbCtx.Close()
		t.usbCtx = nil
	}
}

func (t *LXIOHandler) loop() {
	for {
		select {
		case <-t.ctx.Done():
			return

		default:
			_ = 1
		}

		time.Sleep(1 * time.Millisecond)

		if err := t.pullSensorData(); err != nil {
			logger.Println(err)
		}

		if err := t.pushLightData(); err != nil {
			logger.Println(err)
		}
	}
}

func (t *LXIOHandler) pullSensorData() error {
	d := [16]byte{}

	reqType := uint8(gousb.ControlIn) | uint8(gousb.ControlClass) | uint8(gousb.ControlInterface)
	_, err := t.usbDev.Control(reqType, _HID_GET_REPORT, _HID_REPORT_TYPE_INPUT, 0, d[:])
	if err != nil {
		logger.Println(err)
		return err
	}

	// right left bottom top
	sensor_1p := ^(d[0] & d[1] & d[2] & d[3])
	sensor_2p := ^(d[4] & d[5] & d[6] & d[7])
	sensor_etc := ^d[8]
	sensor_etc2 := ^d[9]

	sensor_hand1p := ^d[10]
	sensor_hand2p := ^d[11]

	sensor := new(LXIOSwitch)
	sensor.P13 = sensor_1p&0b0001_0000 != 0
	sensor.P11 = sensor_1p&0b0000_1000 != 0
	sensor.P15 = sensor_1p&0b0000_0100 != 0
	sensor.P19 = sensor_1p&0b0000_0010 != 0
	sensor.P17 = sensor_1p&0b0000_0001 != 0

	sensor.P23 = sensor_2p&0b0001_0000 != 0
	sensor.P21 = sensor_2p&0b0000_1000 != 0
	sensor.P25 = sensor_2p&0b0000_0100 != 0
	sensor.P29 = sensor_2p&0b0000_0010 != 0
	sensor.P27 = sensor_2p&0b0000_0001 != 0

	sensor.P1Hand3 = sensor_hand1p&0b0001_0000 != 0
	sensor.P1Hand1 = sensor_hand1p&0b0000_1000 != 0
	sensor.P1Hand5 = sensor_hand1p&0b0000_0100 != 0
	sensor.P1Hand9 = sensor_hand1p&0b0000_0010 != 0
	sensor.P1Hand7 = sensor_hand1p&0b0000_0001 != 0

	sensor.P2Hand3 = sensor_hand2p&0b0001_0000 != 0
	sensor.P2Hand1 = sensor_hand2p&0b0000_1000 != 0
	sensor.P2Hand5 = sensor_hand2p&0b0000_0100 != 0
	sensor.P2Hand9 = sensor_hand2p&0b0000_0010 != 0
	sensor.P2Hand7 = sensor_hand2p&0b0000_0001 != 0

	sensor.Clear = sensor_etc&0b1000_0000 != 0
	sensor.Service = sensor_etc&0b0100_0000 != 0
	sensor.Test = sensor_etc&0b0000_0010 != 0
	sensor.Coin = sensor_etc&0b0000_0100 != 0
	// sensor.Coin2 = sensor_etc2&0b0000_0100 != 0

	_ = sensor_etc2

	t.sensorData.Store(sensor)

	return nil
}

func (t *LXIOHandler) pushLightData() error {
	d := [16]byte{}

	light := t.lightData.Load()

	setBit := func(b bool, off int, mask byte) {
		if b {
			d[off] |= mask
		}
	}

	setBit(light.P13, 0, 0b0100_0000)
	setBit(light.P11, 0, 0b0010_0000)
	setBit(light.P15, 0, 0b0001_0000)
	setBit(light.P19, 0, 0b0000_1000)
	setBit(light.P17, 0, 0b0000_0100)

	setBit(light.P23, 2, 0b0100_0000)
	setBit(light.P21, 2, 0b0010_0000)
	setBit(light.P25, 2, 0b0001_0000)
	setBit(light.P29, 2, 0b0000_1000)
	setBit(light.P27, 2, 0b0000_0100)

	setBit(light.P1Hand3, 4, 0b0001_0000)
	setBit(light.P1Hand1, 4, 0b0000_1000)
	setBit(light.P1Hand5, 4, 0b0000_0100)
	setBit(light.P1Hand9, 4, 0b0000_0010)
	setBit(light.P1Hand7, 4, 0b0000_0001)

	setBit(light.P2Hand3, 5, 0b0001_0000)
	setBit(light.P2Hand1, 5, 0b0000_1000)
	setBit(light.P2Hand5, 5, 0b0000_0100)
	setBit(light.P2Hand9, 5, 0b0000_0010)
	setBit(light.P2Hand7, 5, 0b0000_0001)

	// unknown
	setBit(light.Unk1, 1, 0b0000_0100)
	setBit(light.Unk2, 2, 0b1000_0000)
	setBit(light.CoinEnable1, 3, 0b0100_0000)
	setBit(light.CoinEnable2, 3, 0b0010_0000)
	setBit(light.CoinEnable3, 3, 0b0001_0000)
	setBit(light.CoinReceived, 3, 0b0000_1000)
	setBit(light.Unk6, 3, 0b0000_0100)
	setBit(light.Unk7, 3, 0b0000_0010)
	setBit(light.Unk8, 3, 0b0000_0001)

	reqType := uint8(gousb.ControlOut) | uint8(gousb.ControlClass) | uint8(gousb.ControlInterface)
	_, err := t.usbDev.Control(reqType, _HID_SET_REPORT, _HID_REPORT_TYPE_OUTPUT, 0, d[:])
	if err != nil {
		logger.Println(err)
		return err
	}

	return nil
}

func (t *LXIOHandler) SensorData() LXIOSwitch {
	return *t.sensorData.Load()
}

func (t *LXIOHandler) SetLightData(d LXIOLight) {
	t.lightData.Store(&d)
}

func (t LXIOSwitch) UInt32() uint32 {
	n := uint32(0)

	setBit := func(b bool, bit int) {
		if b {
			n |= 1 << bit
		}
	}

	setBit(t.P13, 31)
	setBit(t.P11, 30)
	setBit(t.P15, 29)
	setBit(t.P19, 28)
	setBit(t.P17, 27)

	setBit(t.P23, 26)
	setBit(t.P21, 25)
	setBit(t.P25, 24)
	setBit(t.P29, 23)
	setBit(t.P27, 22)

	setBit(t.P1Hand3, 21)
	setBit(t.P1Hand1, 20)
	setBit(t.P1Hand5, 19)
	setBit(t.P1Hand9, 18)
	setBit(t.P1Hand7, 17)

	setBit(t.P2Hand3, 16)
	setBit(t.P2Hand1, 15)
	setBit(t.P2Hand5, 14)
	setBit(t.P2Hand9, 13)
	setBit(t.P2Hand7, 12)

	setBit(t.Clear, 11)
	setBit(t.Service, 10)
	setBit(t.Test, 9)
	setBit(t.Coin, 8)

	return n
}

func (t *LXIOLight) FromUint32(n uint32) {
	GetBit := func(bit int) int {
		return int((n >> bit) & 1)
	}

	t.P13 = GetBit(31) != 0
	t.P11 = GetBit(30) != 0
	t.P15 = GetBit(29) != 0
	t.P19 = GetBit(28) != 0
	t.P17 = GetBit(27) != 0

	t.P23 = GetBit(26) != 0
	t.P21 = GetBit(25) != 0
	t.P25 = GetBit(24) != 0
	t.P29 = GetBit(23) != 0
	t.P27 = GetBit(22) != 0

	t.P1Hand3 = GetBit(21) != 0
	t.P1Hand1 = GetBit(20) != 0
	t.P1Hand5 = GetBit(19) != 0
	t.P1Hand9 = GetBit(18) != 0
	t.P1Hand7 = GetBit(17) != 0

	t.P2Hand3 = GetBit(16) != 0
	t.P2Hand1 = GetBit(15) != 0
	t.P2Hand5 = GetBit(14) != 0
	t.P2Hand9 = GetBit(13) != 0
	t.P2Hand7 = GetBit(12) != 0

	t.CoinEnable1 = GetBit(11) != 0
	t.CoinEnable2 = GetBit(10) != 0
	t.CoinEnable3 = GetBit(9) != 0
	t.CoinReceived = GetBit(8) != 0
}
