// original code https://github.com/therathatter/piu-kb-hook/blob/main/hook.cpp

#include <iostream>
#include <dlfcn.h>
#include <cstdlib>
#include <linux/input.h>
#include <list>
#include <cstring>

#include "structs.h"
#include "cgo.h"

const uint16_t VENDOR = 0x0547;
const uint16_t PRODUCT = 0x1002;

const uint16_t LXIO_VENDOR = 0x0D2F;
const uint16_t LXIO_PRODUCT = 0x1020;

bool is_lxio_connected = false;
bool was_emulated_device_added = false;

void add_emulated_device() {
	if (!usb_busses) {
		std::cout << "usb_busses was NULL" << std::endl;
		return;
	}

	for (usb_bus *bus = usb_busses; bus; bus = bus->next) {
		for (usb_device *device = bus->devices; device; device = device->next) {
			if (device->descriptor.idVendor == LXIO_VENDOR && device->descriptor.idProduct == LXIO_PRODUCT) {
				is_lxio_connected = true;
				break;
			}
		}

		if (is_lxio_connected) {
			break;
		}
	}

	if (!is_lxio_connected) {
		std::cout << "LXIO device not found." << std::endl;
		return;
	}

	std::cout << "LXIO device found." << std::endl;

	usb_device *emu_device = new usb_device;
	emu_device->config = new usb_config_descriptor;
	emu_device->config->interface = new usb_interface;
	emu_device->descriptor.idVendor = VENDOR;
	emu_device->descriptor.idProduct = PRODUCT;

	LIST_ADD(usb_busses->devices, emu_device);

	was_emulated_device_added = true;
	std::cout << "Added emulated device." << std::endl;
}


extern "C" usb_dev_handle * usb_open(usb_device * dev) {
	static auto o_usb_open = reinterpret_cast<decltype(&usb_open)>(dlsym(RTLD_NEXT, "usb_open"));

	usb_dev_handle *ret = nullptr;
	if (!is_lxio_connected) {
		ret = o_usb_open(dev);
	}
	else {
		ret = new usb_dev_handle;
	}

	return ret;
}


extern "C" int usb_claim_interface(usb_dev_handle * dev, int interface) {
	static auto o_usb_claim_interface = reinterpret_cast<decltype(&usb_claim_interface)>(dlsym(RTLD_NEXT, "usb_claim_interface"));

	if (!o_usb_claim_interface) {
		std::cerr << "o_usb_claim_interface was NULL" << std::endl;
		std::exit(1);
	}

	int ret = 0;
	if (!is_lxio_connected) {
		ret = o_usb_claim_interface(dev, interface);
	}

	return ret;
}

extern "C" int usb_find_busses() {
	static auto o_usb_find_busses = reinterpret_cast<decltype(&usb_find_busses)>(dlsym(RTLD_NEXT, "usb_find_busses"));

	if (!o_usb_find_busses) {
		std::cerr << "o_usb_find_busses was NULL" << std::endl;
		std::exit(1);
	}

	int ret = 0;
	if (!is_lxio_connected) {
		ret = o_usb_find_busses();
	}

	return ret;
}

extern "C" int usb_find_devices() {
	static auto o_usb_find_devices = reinterpret_cast<decltype(&usb_find_devices)>(dlsym(RTLD_NEXT, "usb_find_devices"));

	if (!o_usb_find_devices) {
		std::cerr << "o_usb_find_busses was NULL" << std::endl;
		std::exit(1);
	}

	int ret = 0;
	if (!was_emulated_device_added) {
		ret = o_usb_find_devices();
		add_emulated_device();

		if (is_lxio_connected) {
			ret = 1;
		}
	}

	return ret;
}

extern "C" int usb_set_configuration(usb_dev_handle * dev, int configuration) {
	static auto o_usb_set_configuration = reinterpret_cast<decltype(&usb_set_configuration)>(dlsym(RTLD_NEXT, "usb_set_configuration"));

	if (!o_usb_set_configuration) {
		std::cerr << "o_usb_set_configuration was NULL" << std::endl;
		std::exit(1);
	}

	int ret = 0;
	if (!is_lxio_connected) {
		ret = o_usb_set_configuration(dev, configuration);
	}

	return ret;
}

extern "C" int usb_control_msg(usb_dev_handle * dev, int requesttype, int request, int value, int index, char *bytes, int size, int timeout) {
	static auto o_usb_control_msg = reinterpret_cast<decltype(&usb_control_msg)>(dlsym(RTLD_NEXT, "usb_control_msg"));
	static uint8_t sensorDirection = 0;
	static uint8_t handButtonLightData[2]{};

	if (!o_usb_control_msg) {
		std::cerr << "o_usb_control_msg was NULL" << std::endl;
		std::exit(1);
	}

	int ret = 8;
	if (!is_lxio_connected) {
		return o_usb_control_msg(dev, requesttype, request, value, index, bytes, size, timeout);
	}

	if (requesttype & 0x80) {
		// LIBUSB_ENDPOINT_IN
		memset(static_cast<void *>(bytes), 0xff, size);

		auto sensorData = GetSensorData();
		if (!sensorDirection) {
			// p1
			bytes[0] &= ~((sensorData & (0x1f << 27)) >> 27);

			// p1 hand button
			auto p1HandButton = (sensorData & (0x1f << 17)) >> 17;
			bytes[0] &= ~p1HandButton;

			// p2
			bytes[2] &= ~((sensorData & (0x1f << 22)) >> 22);

			// p2 hand button
			auto p2HandButton = (sensorData & (0x1f << 12)) >> 12;
			bytes[2] &= ~(p2HandButton);

			handButtonLightData[0] = p1HandButton;
			handButtonLightData[1] = p2HandButton;
		}

		// clear
		bytes[1] &= ~((sensorData & (1 << 11)) >> (11 - 7));

		// service
		bytes[1] &= ~((sensorData & (1 << 10)) >> (10 - 6));

		// test
		bytes[1] &= ~((sensorData & (1 << 9)) >> (9 - 1));

		// coin
		bytes[1] &= ~((sensorData & (1 << 8)) >> (8 - 2));
	}
	else {
		// LIBUSB_ENDPOINT_OUT
		sensorDirection = bytes[0] & 0x3;

		uint32_t lightData = 0;

		// 1p
		lightData |= (static_cast<uint32_t>(bytes[0]) & (0x1f << 2)) << (27 - 2);

		// 2p
		lightData |= (static_cast<uint32_t>(bytes[2]) & (0x1f << 2)) << (22 - 2);

		// 1p hand button
		lightData |= static_cast<uint32_t>(handButtonLightData[0]) << 17;

		// 2p hand button
		lightData |= static_cast<uint32_t>(handButtonLightData[1]) << 12;

		// coin enable
		if (static_cast<uint32_t>(bytes[3]) & (1 << 3)) {
			lightData |= 0x7 << 9;
		}

		// coin received
		lightData |= (static_cast<uint32_t>(bytes[3]) & (1 << 4)) << (8 - 4);

		SetLightData(lightData);
	}

	return ret;
}
