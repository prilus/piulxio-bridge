package main

import (
	"context"
	"log"
	"os"
	"time"

	piulxiobridge "gitlab.com/prilus/piulxio-bridge"
)

import "C"

var logger = log.New(os.Stdout, "bridge ", log.LstdFlags|log.Lshortfile)
var handler = (*piulxiobridge.LXIOHandler)(nil)
var fd = (*os.File)(nil)

func init() {
	_fd, err := os.OpenFile("/SETTINGS/log/bridge.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		logger.Println(err)
		return
	}

	fd = _fd

	go func() {
		for {
			time.Sleep(1 * time.Second)
			fd.Sync()
		}
	}()

	logger.SetOutput(fd)

	logger.Println("init")
	ctx := context.Background()

	_handler, err := piulxiobridge.NewLXIOHandler(ctx)
	if err != nil {
		logger.Println(err)
		return
	}

	handler = _handler

	logger.Println("init done")
}

//export GetSensorData
func GetSensorData() uint32 {
	return handler.SensorData().UInt32()
}

//export SetLightData
func SetLightData(data uint32) {
	d := piulxiobridge.LXIOLight{}
	d.FromUint32(data)

	handler.SetLightData(d)
}

func main() {

}
