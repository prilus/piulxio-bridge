package main

import (
	"context"
	"log"
	"os"
	"time"

	piulxiobridge "gitlab.com/prilus/piulxio-bridge"
)

var logger = log.New(os.Stdout, "test ", log.LstdFlags|log.Lshortfile)

func main() {
	ctx := context.Background()
	handler, err := piulxiobridge.NewLXIOHandler(ctx)

	logger.Println("start")

	if err != nil {
		logger.Println(err)
		return
	}

	logger.Println("LXIOHandler created")

	for {
		time.Sleep(1 * time.Millisecond)

		sensorData := handler.SensorData()

		handler.SetLightData(sensorData.LXIOSwitchLightable)
	}
}
