# Pump It Up LXIO Bridge

Simulates LXIO as TXIO

Tested on Fiesta 2

Coin input has not been tested

### build

```bash
cd bridge
GOARCH=386 go tool cgo -exportheader cgo.h main.go 
CGO_ENABLED=1 GOARCH=386 go build -buildmode=c-archive -o libbridge.a .
g++ -shared bridge.cpp -L . -lbridge -lusb-1.0 -o liblxiobridge.so
```

### usage

After building the library, add it to LD_PRELOAD env

### original code

This repo is forked from https://github.com/therathatter/piu-kb-hook