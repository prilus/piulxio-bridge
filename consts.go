package piulxiobridge

const (
	_LXIO_VID = 0x0D2F
	_LXIO_PID = 0x1020

	_HID_GET_REPORT         = 0x01
	_HID_SET_REPORT         = 0x09
	_HID_REPORT_TYPE_INPUT  = 0x0100
	_HID_REPORT_TYPE_OUTPUT = 0x0200
)
